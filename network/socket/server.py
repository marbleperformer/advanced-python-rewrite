import socket

import select

import settings

import collections


class EchoServer():

    def __init__(self):

        # Создаем список для хранения клиентских соединений
        self._connections = list()

        # Создаем список для хранения клиентских запросов
        self._requests = collections.deque()

        self._sock = socket.socket()

        self._sock.bind((settings.HOST, settings.PORT))

        self._sock.listen(settings.CLIENTS_NUM)

        self._sock.settimeout(settings.TIMEOUT)


    def connect(self):

        try:

            client, address = self._sock.accept()

            # Сохраняем подключение клиента
            self._connections.append(client)

        except OSError:

            # Обрабатываем timeout сервера
            pass


    def read(self, client):

        try:

            data = client.recv(settings.BUFFER_SIZE)

            if data:

                str_data = data.decode(settings.ENCODING)

                self._requests.append(str_data)

        except ConnectionResetError:

            if client in self._connections:

                self._connections.remove(client)


    def write(self, client, request):

        try:

            bytes_message = request.encode(settings.ENCODING)

            client.send(bytes_message)

        except (ConnectionResetError, BrokenPipeError):

            if client in self._connections:

                self._connections.remove(client)


    def mainloop(self):

        try:

            while True:

                self.connect()

                if self._requests:

                    request = self._requests.popleft()

                for client in self._connections:

                    self.read(client)

                    self.write(client, request)

        except KeyboardInterrupt:

            pass


if __name__ == '__main__':

    server = Server()

    server.mainloop()
