import socket

import settings


class Client():

    def __init__(self):

        self._sock = socket.socket()

        self._sock.connect((settings.HOST, settings.PORT))


    def read(self, sock):

        bytes_data = self._sock.recv(settings.BUFFER_SIZE)

        str_data = bytes_data.decode(settings.ENCODING)

        print(str_data)


    def write(self):

        str_data = input('Enter data: ')

        bytes_data = str_data.encode(settings.ENCODING)

        self._sock.send(bytes_data)


    def run(self):

        try:

            self.write()

            self.read(sock)

            self._sock.close()

        except KeyboardInterrupt:

            pass


if __name__ == '__main__':

     client = Client()

     client.run()
