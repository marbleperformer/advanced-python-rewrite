import json

import socket

import settings


if __name__ == '__main__':

    with socket.socket() as sock:

        sock.connect((settings.HOST, settings.PORT))

        while True:

            data = sock.recv(settings.BUFFER_SIZE)

            str_data = data.decode(settings.ENCODING)

            messages = json.loads(str_data)

            for address, message in messages.items():

                print('\n', address, '\n', message, '\n', '='*50)

