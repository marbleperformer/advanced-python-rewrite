import json

import socket

import select

import settings


if __name__ == '__main__':

    # Создаем словарь для хранения подключений клиентов
    connections = dict()

    with socket.socket() as sock:

        sock.bind((settings.HOST, settings.PORT))

        sock.listen(settings.CLIENTS_NUM)

        # Определяем timeout сервера
        sock.settimeout(settings.TIMEOUT)

        while True:

            # Создаем словарь для хранения запросов клиентов
            requests = dict()

            try:

                client, address = sock.accept()

                # Сохраняем подключение клиента
                connections.update({address:client})

            except OSError:

                # Обрабатываем timeout сервера
                pass

            # Создаем копию словаря подключений для возможности в дальнейшем вносить изменения в оригинал
            work_copy = connections.copy()

            # Извлекаем коллекцию клиентских socket-объектов
            clients = work_copy.values()

            # Определяем коллекции готовых к записи или чтению клиентских socket-объектов
            rlist, wlist, xist = select.select(clients, clients, [], 0)

            for address, client in work_copy.items():

                try:

                    # Если клиентский socket-объект готов к чтению, получаем данные от клиента
                    if client in rlist:

                        str_address = address[0] + ':' + str(address[1])

                        data = client.recv(settings.BUFFER_SIZE)

                        if data:

                            str_data = data.decode(settings.ENCODING)

                            requests.update({str_address:str_data})

                    # Если клиентский socket-объект готов к записи, отправляем сообщения на клиент
                    if client in wlist:

                        if requests:

                            message = json.dumps(requests)

                            bytes_message = message.encode(settings.ENCODING)

                            client.send(bytes_message)

                except (ConnectionResetError, BrokenPipeError):

                    # Удаляем отключившихся во время записи или чтения клиентские socket-объекты
                    del connections[address]
