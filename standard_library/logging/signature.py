import logging


'''
  В данном модуле приведены примеры использования функции logging с различной сигнатурой
'''



# Определяем формат сообщений
FORMAT = '%(asctime)s - %(message)s'

# определяем базовую конфигурацию модуля logging
logging.basicConfig(format=FORMAT, level='DEBUG')

# создаем экземпляр объекта Logger с именем simple
logger = logging.getLogger('simple')

# Помещаем форматированное сообщение в лог
logger.debug('some message with argument: %s', 'No named argument')


import logging

FORMAT = '%(asctime)s - %(message)s'

logging.basicConfig(format=FORMAT, level='ERROR')

logger = logging.getLogger('exception')

try:

    raise Exception('Simple exception') 

except Exception as err:

    # Помещаем сообщение и информацию о соответствующей ошибке в лог
    logger.exception('Message with exception info.', exc_info=err)


import logging

FORMAT = '%(asctime)s - %(message)s'

logging.basicConfig(format=FORMAT, level='DEBUG')

logger = logging.getLogger('stack')

def function(arg1):

    # Помещаем сообщение и информацию о стеке вызовов в лог
    logger.debug('Message with stack.', stack_info=True)

function('some argument')


FORMAT = '%(asctime)s - %(message)s - %(extrakey)s'

logging.basicConfig(format=FORMAT, level='DEBUG')

logger = logging.getLogger('extra')

extra_content = {'extrakey':'extra value'}

logger.debug('Debug message', extra=extra_content)


FORMAT = '%(name)s - %(levelname)s - %(message)s - %(asctime)s'

logging.basicConfig(format=FORMAT, level='DEBUG')

logger = logging.getLogger('format')

logger.debug('Some format text')

