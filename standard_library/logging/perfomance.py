import time

import logging

FORMAT = '%(asctime)s - %(message)s'

logging.basicConfig(format=FORMAT, level='DEBUG')

logger = logging.getLogger('perfomance')


def simple_log():

    count = 1000

    while count:

        count -= 1

    logger.debug('simple log')


def log_decorator(func):

    def wrap(*args, **kwargs):

        result = func(*args, **kwargs)

        logger.debug('log decorator')
            
        return result

    return wrap


@log_decorator
def decorated_log():

    count = 1000

    while count:

        count -= 1


start = time.time()

simple_log()

print(time.time() - start)

print('='*50)

start = time.time()

decorated_log()

print(time.time() - start)
