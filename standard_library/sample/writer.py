import socket

import settings


if __name__ == '__main__':

    with socket.socket() as sock:

        sock.connect((settings.HOST, settings.PORT))

        while True:

            str_data = input('Enter data: ')

            bytes_data = str_data.encode(settings.ENCODING)

            sock.send(bytes_data)