import json

import socket

import select

import settings



if __name__ == '__main__':

    # Создаем словарь для хранения подключений клиентов
    connections = list()

    with socket.socket() as sock:

        sock.bind((settings.HOST, settings.PORT))

        sock.listen(settings.CLIENTS_NUM)

        # Определяем timeout сервера
        sock.settimeout(settings.TIMEOUT)

        while True:

            # Создаем словарь для хранения запросов клиентов
            requests = list()

            try:

                client, address = sock.accept()

                # Сохраняем подключение клиента
                connections.append(client)

            except OSError:

                # Обрабатываем timeout сервера
                pass

            # Определяем коллекции готовых к записи или чтению клиентских socket-объектов
            rlist, wlist, xist = select.select(connections, connections, [], 0)

            for client in connections:

                try:

                    # Если клиентский socket-объект готов к чтению, получаем данные от клиента
                    if client in rlist:

                        data = client.recv(settings.BUFFER_SIZE)

                        if data:

                            str_data = data.decode(settings.ENCODING)

                            requests.append(str_data)

                    # Если клиентский socket-объект готов к записи, отправляем сообщения на клиент


                    if client in wlist:

                        if requests:

                            message = {'messages':requests}

                            str_message = json.dumps(message)

                            bytes_message = str_message.encode(settings.ENCODING)

                            client.send(bytes_message)

                except (ConnectionResetError, BrokenPipeError):

                    # Удаляем отключившихся во время записи или чтения клиентские socket-объекты
                    connections.remove(client)
