import sys

import zlib


# Создаем исходные данные
str_data = "Some sample data" * 10000

bytes_data = str_data.encode()

bytes_data = str_data.encode('utf-8')

# Компрессия данных
comp_data = zlib.compress(bytes_data)

# Декомпрессия данных
decomp_data = zlib.decompress(comp_data)


# Оценка занимаемого данными пространства


size = sys.getsizeof(str_data)
print(size)
# Результат выполнения функции 160049

size = sys.getsizeof(bytes_data)
print(size)
# Результат выполнения функции 160033

size = sys.getsizeof(comp_data)
print(size)
# Результат выполнения функции 385

size = sys.getsizeof(decomp_data)
print(size)
# Результат выполнения функции 160033
