import json

import socket

import select

import threading

import collections

import settings


class Server():

    def __init__(self):

        self._connections = list()

        self._requests = collections.deque()

        self._sock = socket.socket()

        self._sock.bind((settings.HOST, settings.PORT))

        self._sock.listen(settings.CLIENTS_NUM)

        self._sock.settimeout(settings.TIMEOUT)


    def connect(self):

        try:

            client, address = self._sock.accept()

            # Сохраняем подключение клиента
            self._connections.append(client)

        except OSError:

            # Обрабатываем timeout сервера
            pass


    def read(self, client):

        try:

            data = client.recv(settings.BUFFER_SIZE)

            if data:

                str_data = data.decode(settings.ENCODING)

                self._requests.append(str_data)

        except ConnectionResetError:

            # Удаляем отключившихся во время записи или чтения клиентские socket-объекты
            self._connections.remove(client)


    def write(self, client, request):

        try:

            bytes_message = request.encode(settings.ENCODING)

            client.send(bytes_message)

        except BrokenPipeError:

            # Удаляем отключившихся во время записи или чтения клиентские socket-объекты
            self._connections.remove(client)


    def mainloop(self):

        try:

            while True:

                self.connect()

                # Определяем коллекции готовых к записи или чтению клиентских socket-объектов
                rlist, wlist, xist = select.select(self._connections, self._connections, [], 0)

                for client in rlist:

                    thread = threading.Thread(target=self.read, args=(client,), daemon=True)

                    thread.start()

                if self._requests:

                    request = self._requests.popleft()

                    for client in wlist:

                        thread = threading.Thread(target=self.write, args=(client, request), daemon=True)

                        thread.start()

        except KeyboardInterrupt:

            pass


if __name__ == '__main__':

    server = Server()

    server.mainloop()