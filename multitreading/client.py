import json

import socket

import settings

import threading


class Client():

    def __init__(self):

        self._sock = socket.socket()

        self._sock.connect((settings.HOST, settings.PORT))


    def read(self):

        while True:

            data = self._sock.recv(settings.BUFFER_SIZE)

            str_data = data.decode(settings.ENCODING)

            envelope = json.loads(str_data)

            address = envelope.get('address')

            message = envelope.get('message')

            print('\n', address, '\n', message, '\n', '='*50)


    def write(self):

        while True:

            str_data = input('Enter data: ')

            address = self._sock.getpeername()

            data = json.dumps({'address':address,'message':str_data})

            bytes_data = data.encode(settings.ENCODING)

            self._sock.send(bytes_data)


    def run(self):

        try:

            thread = threading.Thread(target=self.read, daemon=True)

            thread.start()

            self.write()

        except KeyboardInterrupt:

            pass
            

if __name__ == '__main__':

    client = Client()

    client.run()
